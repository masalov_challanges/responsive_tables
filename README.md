# Installation

You need to have a nodejs server installed. See [nodejs official web site](https://nodejs.org/en/)

Once you have nodejs installed. Run the following command to install yarn globally.
> I personally prefer `yarn` as it have multiple advantages over `npm`.
  But you can install everything using `npm install` command as well.
```
npm install -g yarn
```

Navigate to the project folder and install node modules.
```
yarn install
```

I personally prefer `yarn` as it have multiple advantages over `npm`.
But you can install everythin using `npm install` command as well.

# Building and Running project

Simply kick off the server, it will compile a production build and start a local server.

```
yarn start
```
> It will start local server on 8080 port. As an option start the server and click this [link](http://localhost:8080)

Development build can be run by
```
yarn run develop
```

# Jest testing

To run snapshot JEST tests
```
yarn test
```