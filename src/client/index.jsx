import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routes.jsx';
import './style.css';

ReactDOM.render(<Routes />, document.getElementById('app'));