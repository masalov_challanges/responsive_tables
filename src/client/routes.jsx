import React from 'react';
import {Provider} from 'react-redux';
import {Router, Route, browserHistory} from 'react-router';
import {routerMiddleware, syncHistoryWithStore, routerReducer} from 'react-router-redux';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import reducers from './redux/reducers';
import thunkMiddleware from 'redux-thunk';

/**
 * Introduce chunks by splitting code into multiple files
 * @see https://webpack.github.io/docs/code-splitting.html
 */
if (typeof require.ensure !== 'function') require.ensure = function requireEnsure(d, c) {
	c(require);
};

function getMainLayout(location, callback) {
	require.ensure([], function (require) {
		callback(null, require('./modules/main/Main.jsx').default);
	}, 'main');
}

const reducer = combineReducers({
	app: reducers,
	routing: routerReducer
});

const middleware = [thunkMiddleware, routerMiddleware(browserHistory)];
const store = createStore(
	reducer,
	__DEV_MODE__ && __DEVTOOLS__ ? window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() : false,
	applyMiddleware(...middleware)
);

const history = syncHistoryWithStore(browserHistory, store);
const Routes = () => {
	return <Provider store={store}>
		<Router history={history}>
			<Route path='/' getComponent={getMainLayout} />
		</Router>
	</Provider>;
};

export default Routes;