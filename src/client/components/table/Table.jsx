import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './Table.scss';

/**
 * Create an array of numbers to render later;
 *
 * @param start
 * @param end
 * @param step
 * @param length
 * @param direction
 * @returns {Array}
 */
function constructTableArray(start, end, step, direction, length) {
	const result = [];

	let y = 0;
	let current = start;
	let hasRow = true;
	while (hasRow) {
		let cells = [];
		for (let x = 0; x < length; x += 1) {

			if (step > 0) {
				if (current <= end) {
					cells.push(current);
					current += step;
				} else {
					hasRow = false;
					cells.push(null);
				}
			} else {
				if (current >= end) {
					cells.push(current);
					current += step;
				} else {
					hasRow = false;
					cells.push(null);
				}
			}
		}

		if (y % 2) {
			if (direction === 'LTR-UP') {
				cells = cells.reverse();
			}
		} else {
			if (direction === 'RTL-UP') {
				cells = cells.reverse();
			}
		}

		result.unshift(cells);
		y += 1;
	}

	return result;
} // constructTableArray

/**
 * Stateless component to render Table
 *
 * @param className
 * @param start
 * @param end
 * @param width
 * @param step
 * @param direction
 * @param columns
 */
const Table = ({className, start, end, width, step = 1, direction = 'LTR-UP', columns = 5, name, onConfigClick}) => {
	if (step < 0 && start <= end) {
		throw new Error('end should be less than start if step < 0');
	}

	if (step > 0 && start >= end) {
		throw new Error('end should be bigger than start if step > 0');
	}

	if (step === 0) {
		throw new Error('step 0 is not allowed');
	}

	const array = constructTableArray(start, end, step, direction, columns);
	return <div className={classNames('table-wrapper', className)} style={{width: `${width}%`}}>
		<table>
			<tbody>
			{
				array.map((row, yIndex) => {
					return <tr key={`${name}-${yIndex}`}>
						{
							row.map((value, xIndex) => {
								return <td key={`${name}-${yIndex}-${xIndex}`} className={ value !== null ? '' : 'empty'}>{ value }</td>;
							})
						}
					</tr>;
				})
			}
			</tbody>
		</table>
		<button onClick={ () => onConfigClick(name) }>Configure</button>
		<span className="float-right">{`${width}%`}</span>
	</div>;
}; // Table

const {string, number, oneOf, func} = PropTypes;
Table.propTypes = {
	start: number.isRequired,
	end: number.isRequired,
	width: number.isRequired,
	step: number,
	direction: oneOf([
		'LTR-UP',
		'RTL-UP'
	]),
	columns: number,
	className: string,
	name: string,
	onConfigClick: func
};

export default Table;