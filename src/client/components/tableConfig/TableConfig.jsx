import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './TableConfig.scss';

const {string, number, shape, oneOf, func} = PropTypes;
class TableConfig extends Component {
	static propTypes = {
		className: string,
		name: string.isRequired,
		values: shape({
			start: number.isRequired,
			end: number.isRequired,
			width: number.isRequired,
			step: number,
			direction: oneOf([
				'LTR-UP',
				'RTL-UP'
			])
		}).isRequired,

		onSubmit: func,
		onCancel: func
	};

	constructor(props) {
		super(props);

		this.state = {
			name: props.name,
			start: props.values.start,
			end: props.values.end,
			width: props.values.width,
			step: props.values.step,
			direction: props.values.direction
		};

		this.getValues = this.getValues.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	getValues() {
		return this.state;
	}

	handleChange(event) {
		if (event.target.id === 'direction') {
			this.setState({[event.target.id]: event.target.value});
		} else {
			this.setState({[event.target.id]: parseInt(event.target.value, 10)});
		}
	}

	render() {
		const {name, values, onSubmit, onCancel, className} = this.props;
		return <section className={classNames('table-config', className)}>
			<h4>Table: <span className={name}>{name}</span></h4>
			<div>
				<label htmlFor="start">N</label>
				<input id="start" type='number' defaultValue={values.start} onChange={ this.handleChange }/>
			</div>
			<div>
				<label htmlFor="step">X</label>
				<input id='step' type='number' defaultValue={values.step} onChange={ this.handleChange }/>
			</div>
			<div>
				<label htmlFor="end">M</label>
				<input id='end' type='number' defaultValue={values.end} onChange={ this.handleChange }/>
			</div>
			<div>
				<label htmlFor="width">W</label>
				<input id='width' max="100" type='number' defaultValue={values.width} onChange={ this.handleChange }/>
				<span className="percent float-right">%</span>
			</div>
			<div>
				<label htmlFor="direction">D</label>
				<div className="direction-wrapper float-right">
					<select id="direction" defaultValue={values.direction} onChange={ this.handleChange }>
						<option value='LTR-UP'>LTR-UP</option>
						<option value='RTL-UP'>RTL-UP</option>
					</select>
				</div>
			</div>
			<button onClick={ () => {
				onSubmit(name, this.state);
			}}>OK
			</button>
			<button onClick={onCancel}>CANCEL</button>
		</section>;
	} // render
}

export default TableConfig;