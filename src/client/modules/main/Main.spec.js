import React from 'react';
import Main from './Main.jsx';
import { createMockStore } from 'redux-test-utils';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
	const expectedState = {
		app: {
			main: {
				showConfig: false,
				configName: 'red',
				red: {
					start: 8,
					end: 29,
					step: 1,
					width: 20,
					direction: 'LTR-UP',
					name: 'red'
				},
				green: {
					start: 231,
					end: 247,
					step: 1,
					width: 30,
					direction: 'LTR-UP',
					name: 'green'
				},
				blue: {
					start: 47,
					end: 81,
					step: 2,
					width: 40,
					direction: 'RTL-UP',
					name: 'blue'
				}
			}
		}
	};
	const tree = renderer.create(
		<Main store={createMockStore(expectedState)} />
	).toJSON();
	expect(tree).toMatchSnapshot();
});
