import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {showConfig, hideConfig, updateValues} from './redux/action';

import Table from './../../components/table/Table.jsx';
import TableConfig from './../../components/tableConfig/TableConfig.jsx';

import './Main.scss';

const {object, func, bool, string} = PropTypes;
class Main extends Component {
	static propTypes = {
		red: object.isRequired,
		green: object.isRequired,
		blue: object.isRequired,
		showConfig: bool,
		configName: string,

		onConfig: func,
		onCancel: func,
		onSubmit: func
	};

	constructor(props) {
		super(props);
	}

	render() {
		let {red, green, blue, onConfig, onCancel, onSubmit, showConfig, configName} = this.props;
		return <section className='main'>
			<Table className='red' { ...red } onConfigClick={ onConfig }/>
			<Table className='green' { ...green } onConfigClick={ onConfig }/>
			<Table className='blue' { ...blue } onConfigClick={ onConfig }/>
			{
				showConfig && <TableConfig
					name={configName}
					values={ this.props[configName] }
					onSubmit={onSubmit}
					onCancel={onCancel}
				/>
			}
		</section>;
	}
}

const mapStateToProps = (state) => {
	let {main: {red, green, blue, configName, showConfig}} = state.app;
	return {
		red,
		green,
		blue,
		configName,
		showConfig
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		onMount() {
			//todo: restore state from locale storage
		},

		onConfig(name) {
			dispatch(showConfig(name));
		},

		onCancel() {
			dispatch(hideConfig());
		},

		onSubmit(name, values) {
			dispatch(updateValues(name, values));
			dispatch(hideConfig());
		}
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Main);