import {SHOW_TABLE_CONFIG, HIDE_TABLE_CONFIG, UPDATE_TABLE} from './action';

export default function main(state = {
	showConfig: false,
	configName: 'red',
	red: {
		start: 8,
		end: 29,
		step: 1,
		width: 20,
		direction: 'LTR-UP',
		name: 'red'
	},
	green: {
		start: 231,
		end: 247,
		step: 1,
		width: 30,
		direction: 'LTR-UP',
		name: 'green'
	},
	blue: {
		start: 47,
		end: 81,
		step: 2,
		width: 40,
		direction: 'RTL-UP',
		name: 'blue'
	}
}, action) {
	if (action) {
		switch (action.type) {

			case SHOW_TABLE_CONFIG:
				return {
					...state,
					showConfig: true,
					configName: action.name
				};

			case HIDE_TABLE_CONFIG:
				return {
					...state,
					showConfig: false
				};

			case UPDATE_TABLE:
				return {
					...state,
					[action.name]: action.values
				};
			default:
				return state;
		}
	}
	return state;
}
