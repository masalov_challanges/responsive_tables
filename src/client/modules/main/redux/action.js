export const SHOW_TABLE_CONFIG = '@@app/SHOW_TABLE_CONFIG';
export const HIDE_TABLE_CONFIG = '@@app/HIDE_TABLE_CONFIG';
export const UPDATE_TABLE = '@@app/UPDATE_TABLE';

export function showConfig(name) {
	return {type: SHOW_TABLE_CONFIG, name};
}

export function hideConfig() {
	return {type: HIDE_TABLE_CONFIG};
}

export function updateValues(name, values) {
	return {type: UPDATE_TABLE, name, values};
}