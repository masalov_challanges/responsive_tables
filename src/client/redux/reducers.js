import {SHOW_TABLE_CONFIG, HIDE_TABLE_CONFIG, UPDATE_TABLE} from './../modules/main/redux/action';
import mainReducer from './../modules/main/redux/reducers';

export default function appReducer(state = {
	main: mainReducer()
}, action) {
	if (action) {
		switch (action.type) {
			case UPDATE_TABLE:
			case HIDE_TABLE_CONFIG:
			case SHOW_TABLE_CONFIG:
				return {
					main: mainReducer(state.main, action)
				};
			default:
				return state;
		}
	}
	return state;
}
